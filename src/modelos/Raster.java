package modelos;

import java.awt.*;
import java.awt.image.*;

public class Raster {

    private int width;
    private int height;
    private int pixel[];

    public Raster(int w, int h) {
        width = w;
        height = h;
        pixel = new int[w * h];
    }

    /**
     * Este constructor crea un Raster inicializado con el contenido de una
     * imagen
     */
    public Raster(Image img) {

        try {

            PixelGrabber grabber = new PixelGrabber(img, 0, 0, -1, -1, true);

            if (grabber.grabPixels()) {
                width = grabber.getWidth();
                height = grabber.getHeight();
                pixel = (int[]) grabber.getPixels();
            }

        } catch (InterruptedException e) {

        }

    }

    /* Retorna el número de pixeles   */
    public final int size() {
        return getPixel().length;
    }

    /* Rellena el objeto Raster con un color solido */
    public final void fill(Color c) {
        int s = size();
        int rgb = c.getRGB();
        for (int i = 0; i < s; i++) {
            getPixel()[i] = rgb;
        }
    }

    /* Convierte la imagen rasterizada a un objeto Image */
    public final Image toImage(Component root) {
        return root.createImage(new MemoryImageSource(getWidth(), getHeight(), getPixel(), 0, getWidth()));
    }

    /*  Obtiene un color desde un Raster */
    public final Color getColor(int x, int y) {
        return new Color(getPixel()[y * getWidth() + x]);
    }

    /*  Establece un pixel en un valor dado  */
    public final boolean setPixel(int pix, int x, int y) {
        getPixel()[y * getWidth() + x] = pix;
        return true;
    }

    /* Establece un pixel en un color dado */
    public final boolean setColor(Color c, int x, int y) {
        getPixel()[y * getWidth() + x] = c.getRGB();
        return true;
    }

    /**
     * @return the pixel
     */
    public int[] getPixel() {
        return pixel;
    }

    /**
     * @param pixel the pixel to set
     */
    public void setPixel(int[] pixel) {
        this.pixel = pixel;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }
}
