package modelos.figuras;

import java.awt.*;
import modelos.Raster;

public class Circulo extends Figura {

    private Point p1;
    private Point p2;
    private Linea linea;

    public Circulo(Point p1, Point p2, Color c) {
        this.p1 = p1;
        this.p2 = p2;
        color = c;
        linea = new Linea(p1, p2, c);
    }

    public void circuloRelleno(Raster raster) {
        linea.setRaster(raster);
        double dX = Math.pow((Math.abs(p2.x - p1.x)), 2);
        double dY = Math.pow((Math.abs(p2.y - p1.y)), 2);
        int d = (int) (Math.sqrt(dX + dY));
        int centroX = (p1.x + p2.x) / 2;
        int centroY = (p1.y + p2.y) / 2;
        int radio = (d / 2);
        int x = radio;
        int y = 0;
        int radioError = 1 - x;

        while (x >= y) {
            int xInicial = -x + centroX;
            int xFinal = x + centroX;
            linea.dibujarLineFast(xInicial, y + centroY, xFinal, y + centroY, color);
            if (y != 0) {
                linea.dibujarLineFast(xInicial, -y + centroY, xFinal, -y + centroY, color);
            }

            y++;

            if (radioError < 0) {
                radioError += 2 * y + 1;
            } else {
                if (x >= y) {
                    xInicial = -y + 1 + centroX;
                    xFinal = y - 1 + centroX;
                    linea.dibujarLineFast(xInicial, x + centroY, xFinal, x + centroY, color);
                    linea.dibujarLineFast(xInicial, -x + centroY, xFinal, -x + centroY, color);
                }
                x--;
                radioError += 2 * (y - x + 1);
            }

        }
    }

    /**
     * Helper function to draw all 8 symmetric circle points.
     *
     * @param g Graphics object used for actual pixel plotting
     * @param mx x-coordi raster.setPixel(color.getRGB(), x, y); }nate of center
     * of circle
     * @param my y-coordinate of center of circle
     * @param x x-value for point on circle from center
     * @param y y-value for point on circle from center
     * @param multiplier zoom multiplier, use 1 for normal drawing
     */
    private void drawSymmetricPoints(Raster raster, int mx, int my, int x, int y) {
        int c = color.getRGB();
        raster.setPixel(c, (mx + x), (my - y));
        raster.setPixel(c, (mx + x), (my + y));
        raster.setPixel(c, (mx - x), (my + y));
        raster.setPixel(c, (mx - x), (my - y));
        raster.setPixel(c, (mx + y), (my - x));
        raster.setPixel(c, (mx + y), (my + x));
        raster.setPixel(c, (mx - y), (my + x));
        raster.setPixel(c, (mx - y), (my - x));
    }

    /**
     * *************************************************************************
     * Public drawing functions
     * *************************************************************************
     */
    /**
     * Simple approach evaluating the circle equation y = y0 +- sqrt(r^2 -
     * (x-x0)^2) and make only use of 2-way symmetry.
     *
     * @param g Graphics object where circle is plotted
     * @param midPointX x-coordinate of center of circle
     * @param midPointY y-coordinate of center of circle
     * @param radius radius of circle
     * @param multiplier zoom multiplier, use 1 for normal drawing
     */
    public void drawCircleBresenhamInt(Raster g) {

        double dX = Math.pow((Math.abs(p2.x - p1.x)), 2);
        double dY = Math.pow((Math.abs(p2.y - p1.y)), 2);
        int d = (int) (Math.sqrt(dX + dY));
        int radius = d / 2;
        int multiplier = 1;
        int midPointX = (p1.x + p2.x) / 2;
        int midPointY = (p1.y + p2.y) / 2;

        midPointX /= multiplier;
        midPointY /= multiplier;
        radius /= multiplier;
        int x = 0;
        int y = radius;
        d = 1 - radius;

        drawSymmetricPoints(g, midPointX, midPointY, x, y);
        while (x < y) {
            if (d < 0) {
                d = d + 2 * x + 3;
                x++;
            } else {
                d = d + 2 * (x - y) + 5;
                x++;
                y--;
            }
            drawSymmetricPoints(g, midPointX, midPointY, x, y);
        }
    }

    public Point getPunto1() {
        return p1;
    }

    public Point getPunto2() {
        return p2;
    }

}
