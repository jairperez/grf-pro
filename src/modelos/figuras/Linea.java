package modelos.figuras;

import java.awt.*;
import modelos.Raster;

public class Linea extends Figura {

    private Point punto1;
    private Point punto2;
    private Raster raster;

    public Linea(Point p1, Point p2, Color c) {
        punto1 = new Point(p1.x, p1.y);
        punto2 = new Point(p2.x, p2.y);
        color = c;
    }

    /**
     * @return the punto1
     */
    public Point getPunto1() {
        return punto1;
    }

    /**
     * @param punto1 the punto1 to set
     */
    public void setPunto1(Point punto1) {
        this.punto1 = punto1;
    }

    /**
     * @return the punto2
     */
    public Point getPunto2() {
        return punto2;
    }

    /**
     * @param punto2 the punto2 to set
     */
    public void setPunto2(Point punto2) {
        this.punto2 = punto2;
    }

    public void dibujarLineFast(int x0, int y0, int x1, int y1, Color color) {
        int pix = color.getRGB();
        int dy = y1 - y0;
        int dx = x1 - x0;
        int stepx, stepy;
        if (dy < 0) {
            dy = -dy;
            stepy = -raster.getWidth();
        } else {
            stepy = raster.getWidth();
        }
        if (dx < 0) {
            dx = -dx;
            stepx = -1;
        } else {
            stepx = 1;
        }
        dy <<= 1;
        dx <<= 1;
        y0 *= raster.getWidth();
        y1 *= raster.getWidth();
        raster.getPixel()[x0 + y0] = pix;
        if (dx > dy) {
            int fraction = dy - (dx >> 1);
            while (x0 != x1) {
                if (fraction >= 0) {
                    y0 += stepy;
                    fraction -= dx;
                }
                x0 += stepx;
                fraction += dy;
                raster.getPixel()[x0 + y0] = pix;
            }
        } else {
            int fraction = dx - (dy >> 1);
            while (y0 != y1) {
                if (fraction >= 0) {
                    x0 += stepx;
                    fraction -= dy;
                }
                y0 += stepy;
                fraction += dx;
                raster.getPixel()[x0 + y0] = pix;
            }
        }
    }

    public void dibujarLineaMejorada(int x0, int y0, int x1, int y1, Color color) {
        int pix = color.getRGB();
        int dx = x1 - x0;
        int dy = y1 - y0;
        raster.setPixel(pix, x0, y0);
        if (Math.abs(dx) > Math.abs(dy)) {     // inclinacion < 1
            float m = (float) dy / (float) dx; // calcular inclinacion
            float b = y0 - m * x0;
            dx = (dx < 0) ? -1 : 1;
            while (x0 != x1) {
                x0 += dx;
                raster.setPixel(pix, x0, Math.round(m * x0 + b));
            }
        } else {
            if (dy != 0) {                         // inclinacion >= 1
                float m = (float) dx / (float) dy; // Calcular inclinacion
                float b = x0 - m * y0;
                dy = (dy < 0) ? -1 : 1;
                while (y0 != y1) {
                    y0 += dy;
                    raster.setPixel(pix, Math.round(m * y0 + b), y0);
                }
            }
        }
    }

    public void dibujarLineaSimple(int x0, int y0, int x1, int y1, Color color) {
        int pix = color.getRGB();
        int dx = x1 - x0;
        int dy = y1 - y0;

        raster.setPixel(pix, x0, y0);

        if (dx != 0) {
            float m = (float) dy / (float) dx;
            float b = y0 - m * x0;

            dx = (x1 > x0) ? 1 : -1;

            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m * x0 + b);
                raster.setPixel(pix, x0, y0);
            }
        }
    }

    /**
     * @param raster the raster to set
     */
    public void setRaster(Raster raster) {
        this.raster = raster;
    }

}
