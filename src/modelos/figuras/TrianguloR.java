package modelos.figuras;

import java.awt.Color;
import java.awt.Point;
import modelos.*;

public class TrianguloR extends Figura {

    private Vertex2D vertex2D[];
    private int colorInt;

    public TrianguloR() {
    }

    public TrianguloR(Vertex2D v0, Vertex2D v1, Vertex2D v2, Color _color) {
        color = _color;
        colorInt = _color.getRGB();

        vertex2D = new Vertex2D[3];
        vertex2D[0] = v0;
        vertex2D[1] = v1;
        vertex2D[2] = v2;

        int a = ((v0.getArgb() >> 24) & 255) + ((v1.getArgb() >> 24) & 255) + ((v2.getArgb() >> 24) & 255);
        int r = ((v0.getArgb() >> 16) & 255) + ((v1.getArgb() >> 16) & 255) + ((v2.getArgb() >> 16) & 255);
        int g = ((v0.getArgb() >> 8) & 255) + ((v1.getArgb() >> 8) & 255) + ((v2.getArgb() >> 8) & 255);
        int b = (v0.getArgb() & 255) + (v1.getArgb() & 255) + (v2.getArgb() & 255);

        a = (a + a + 3) / 6;
        r = (r + r + 3) / 6;
        g = (g + g + 3) / 6;
        b = (b + b + 3) / 6;

        colorInt = (a << 24) | (r << 16) | (g << 8) | b;
    }

    private EdgeEqn edge[];
    private int area;
    private int xMin, xMax, yMin, yMax;
    private static byte sort[][] = {
        {0, 1}, {1, 2}, {0, 2}, {2, 0}, {2, 1}, {1, 0}
    };

    public void dibujar(Raster r) {
        if (!inicializarTriangulo(r)) {
            return;
        }

        int x, y;
        int A0 = edge[0].getA();
        int A1 = edge[1].getA();
        int A2 = edge[2].getA();

        int B0 = edge[0].getB();
        int B1 = edge[1].getB();
        int B2 = edge[2].getB();

        int t0 = A0 * xMin + B0 * yMin + edge[0].getC();
        int t1 = A1 * xMin + B1 * yMin + edge[1].getC();
        int t2 = A2 * xMin + B2 * yMin + edge[2].getC();

        yMin *= r.getWidth();
        yMax *= r.getWidth();

        for (y = yMin; y <= yMax; y += r.getWidth()) {
            int e0 = t0;
            int e1 = t1;
            int e2 = t2;
            int xflag = 0;
            for (x = xMin; x <= xMax; x++) {
                if ((e0 | e1 | e2) >= 0) {
                    r.getPixel()[y + x] = getColorInt();
                    xflag++;
                } else if (xflag != 0) {
                    break;
                }
                e0 += A0;
                e1 += A1;
                e2 += A2;
            }
            t0 += B0;
            t1 += B1;
            t2 += B2;
        }

    }

    public boolean inicializarTriangulo(Raster r) {
        if (edge == null) {
            edge = new EdgeEqn[3];
        }

        /*
            Compute the three edge equations
         */
        edge[0] = new EdgeEqn(getVertex2D()[0], getVertex2D()[1]);
        edge[1] = new EdgeEqn(getVertex2D()[1], getVertex2D()[2]);
        edge[2] = new EdgeEqn(getVertex2D()[2], getVertex2D()[0]);

        /*
            Truco #1: 
            Oriente los bordes de modo que el 
            interior del triángulo quede dentro 
            de sus semiespacios positivos.
         */
        area = edge[0].getC() + edge[1].getC() + edge[2].getC();
        if (area == 0) {
            return false;                // degenerar el triangulo
        }
        if (area < 0) {
            edge[0].flip();
            edge[1].flip();
            edge[2].flip();
            area = -area;
        }

        /*
            Trick #2: calcular el recuadro que encierra el triangulo
         */
        int xflag = edge[0].getFlag() + 2 * edge[1].getFlag() + 4 * edge[2].getFlag();
        int yflag = (xflag >> 3) - 1;
        xflag = (xflag & 7) - 1;

        xMin = (int) (getVertex2D()[sort[xflag][0]].getX());
        xMax = (int) (getVertex2D()[sort[xflag][1]].getX() + 1);
        yMin = (int) (getVertex2D()[sort[yflag][1]].getY());
        yMax = (int) (getVertex2D()[sort[yflag][0]].getY() + 1);

        /*
            recortar el espacio del triangulo
         */
        xMin = (xMin < 0) ? 0 : xMin;
        xMax = (xMax >= r.getWidth()) ? r.getWidth() - 1 : xMax;
        yMin = (yMin < 0) ? 0 : yMin;
        yMax = (yMax >= r.getHeight()) ? r.getHeight() - 1 : yMax;
        return true;
    }

    /**
     * @return the vertex2D
     */
    public Vertex2D[] getVertex2D() {
        return vertex2D;
    }

    /**
     * @param vertex2D the vertex2D to set
     */
    public void setVertex2D(Vertex2D[] vertex2D) {
        this.vertex2D = vertex2D;
    }

    /**
     * @return the colorInt
     */
    public int getColorInt() {
        return colorInt;
    }

    /**
     * @param colorInt the colorInt to set
     */
    public void setColorInt(int colorInt) {
        this.colorInt = colorInt;
    }
    
    public Point getPunto1() {
        Point p = new Point(vertex2D[0].getX(), vertex2D[0].getY());
        return p;
    }
    
    public Point getPunto2() {
        Point p = new Point(vertex2D[1].getX(), vertex2D[1].getY());
        return p;
    }
    
    public Point getPunto3() {
        Point p = new Point(vertex2D[2].getX(), vertex2D[2].getY());
        return p;
    }

}
