package modelos;

public class EdgeEqn {

    public final static int FRACBITS = 12;
    private int A, B, C;
    private int flag;

    public EdgeEqn(Vertex2D v0, Vertex2D v1) {
        double a = v0.getY() - v1.getY();
        double b = v1.getX() - v0.getX();
        double c = -0.5f * (a * (v0.getX() + v1.getX()) + b * (v0.getY() + v1.getY()));

        A = (int) (a * (1 << FRACBITS));
        B = (int) (b * (1 << FRACBITS));
        C = (int) (c * (1 << FRACBITS));

        flag = 0;
        if (A >= 0) {
            flag += 8;
        }
        if (B >= 0) {
            flag += 1;
        }
    }

    public void flip() {
        A = -A;
        B = -B;
        C = -C;
    }

    public int evaluate(int x, int y) {
        return (A * x + B * y + C);
    }

    /**
     * @return the A
     */
    public int getA() {
        return A;
    }

    /**
     * @return the B
     */
    public int getB() {
        return B;
    }

    /**
     * @return the C
     */
    public int getC() {
        return C;
    }

    /**
     * @return the flag
     */
    public int getFlag() {
        return flag;
    }

}
