package vistas;

import controladores.CBotones;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.*;
import modelos.Raster;

public class PanelControles extends JPanel {

    private Color color;
    private JButton btnGuardarRast, btnColor, btnBorrar, btnTrasladar, btnEscalar, btnRotar;
    private JToggleButton rbLinea, rbTriang, rbRectang, rbCirculo, rbElipse;
    protected JTextField campoTX, campoTY, campoSX, campoSY, campoRA;
    private ButtonGroup bg;
    private Raster raster;

    public PanelControles() {
        addComponentes();
        this.setFocusable(true);
    }

    public final void addComponentes() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        setColor(Color.black);
        //PANEL TRASLADAR
        JPanel panelCamposTrasladar = new JPanel();
        panelCamposTrasladar.setLayout(new FlowLayout());
        
        campoTX = new JTextField(5);
        campoTY = new JTextField(5);
        
        panelCamposTrasladar.add(campoTX);
        panelCamposTrasladar.add(campoTY);
        //PANEL ESCALAR
        JPanel panelCamposEscalar = new JPanel();
        panelCamposEscalar.setLayout(new FlowLayout());
        
        campoSX = new JTextField(5);
        campoSY = new JTextField(5);
        
        panelCamposEscalar.add(campoSX);
        panelCamposEscalar.add(campoSY);
        //PANEL ROTAR
        JPanel panelCampoRotar = new JPanel();
        panelCampoRotar.setLayout(new FlowLayout());
        
        campoRA = new JTextField(5);
        
        panelCampoRotar.add(campoRA);
        
        btnBorrar = new JButton("Borrar");
        btnGuardarRast = new JButton("Guardar Rast");
        btnColor = new JButton("Color");
        btnTrasladar = new JButton("Trasladar");
        btnEscalar = new JButton("Escalar");
        btnRotar = new JButton("Rotar");

        btnBorrar.setName("btnBorrar");
        btnGuardarRast.setName("btnGuardarRast");
        btnColor.setName("btnColor");
        btnTrasladar.setName("btnTrasladar");
        btnEscalar.setName("btnEscalar");
        btnRotar.setName("btnRotar");

        btnColor.setBorderPainted(false);
        btnColor.setFocusPainted(false);

        btnColor.setBackground(getColor());
        btnColor.setForeground(getColor());

        rbLinea = new JToggleButton("Linea");
        rbTriang = new JToggleButton("Triangulo");
        rbRectang = new JToggleButton("Rectangulo");
        rbCirculo = new JToggleButton("Circulo");
        rbElipse = new JToggleButton("Elipse");

        rbLinea.setActionCommand("Linea");
        rbTriang.setActionCommand("Triangulo");
        rbRectang.setActionCommand("Rectangulo");
        rbCirculo.setActionCommand("Circulo");
        rbElipse.setActionCommand("Elipse");
        
        rbLinea.setName("Linea");
        rbTriang.setName("Triangulo");
        rbRectang.setName("Rectangulo");
        rbCirculo.setName("Circulo");
        rbElipse.setName("Elipse");

        bg = new ButtonGroup();
        
        rbLinea.setSelected(true);
        bg.add(rbLinea);
        bg.add(rbTriang);
        bg.add(rbRectang);
        bg.add(rbCirculo);
        bg.add(rbElipse);

        add(rbLinea);
        add(rbTriang);
        add(rbRectang);
        add(rbCirculo);
        add(rbElipse);

        add(new JSeparator());
        add(btnTrasladar);
        add(panelCamposTrasladar);
        
        add(new JSeparator());
        add(btnEscalar);
        add(panelCamposEscalar);
        
        add(new JSeparator());
        add(btnRotar);
        add(panelCampoRotar);
        
        add(new JSeparator());
        add(btnBorrar);
        add(btnColor);
        add(btnGuardarRast);
    }
    
    public void addEventos(CBotones cBotones) {
        btnGuardarRast.addActionListener(cBotones);
        btnColor.addActionListener(cBotones);
        rbLinea.addActionListener(cBotones);
        rbTriang.addActionListener(cBotones);
        btnBorrar.addActionListener(cBotones);
        rbRectang.addActionListener(cBotones);
        rbCirculo.addActionListener(cBotones);
        rbElipse.addActionListener(cBotones);
        btnTrasladar.addActionListener(cBotones);
        btnRotar.addActionListener(cBotones);
        btnEscalar.addActionListener(cBotones);
        this.addKeyListener(cBotones);
    }

    /**
     * @return the btnColor
     */
    public JButton getBtnColor() {
        return btnColor;
    }

    /**
     * @return the rbLinea
     */
    public ButtonGroup getGrupoBotones() {
        return bg;
    }

    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }
    
    /**
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * @return the raster
     */
    public Raster getRaster() {
        return raster;
    }

    /**
     * @return the campoTX
     */
    public JTextField getCampoTX() {
        return campoTX;
    }

    /**
     * @param campoTX the campoTX to set
     */
    public JTextField getCampoTY() {
        return campoTY;
    }

    /**
     * @return the campoSX
     */
    public JTextField getCampoSX() {
        return campoSX;
    }

    /**
     * @return the campoSY
     */
    public JTextField getCampoSY() {
        return campoSY;
    }

    /**
     * @return the campoRA
     */
    public JTextField getCampoRA() {
        return campoRA;
    }

}
