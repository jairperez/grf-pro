package vistas;

import controladores.*;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.*;
import modelos.Raster;
import modelos.figuras.*;

public class PanelPrincipal extends JPanel {

    private final Raster raster;
    private PanelControles panelControles;
    private PanelFiguras panelFiguras;
    private PanelRaster panelRaster;
    private final ArrayList<Figura> aFiguras;

    public PanelPrincipal(Raster raster) {
        aFiguras = new ArrayList<Figura>(); 
        
        this.raster = raster;
        addComponentes();

        CBotones cBotones = new CBotones(raster, this);
        panelControles.addEventos(cBotones);
        panelFiguras.addEventos(cBotones);
        
        CPanelRaster cPanelRaster = new CPanelRaster(raster, this);
        panelRaster.addEventos(cPanelRaster);
    }

    public final void addComponentes() {
        setLayout(new BorderLayout());

        panelControles = new PanelControles();
        add(panelControles, BorderLayout.WEST);

        panelRaster = new PanelRaster(raster);
        getPanelRaster().setBackground(Color.WHITE);
        add(panelRaster, BorderLayout.CENTER);

        panelFiguras = new PanelFiguras();
        add(panelFiguras, BorderLayout.EAST);
    }

    /**
     * @return the panelControles
     */
    public PanelControles getPanelControles() {
        return panelControles;
    }

    /**
     * @return the panelFiguras
     */
    public PanelFiguras getPanelFiguras() {
        return panelFiguras;
    }

    /**
     * @return the panelRaster
     */
    public PanelRaster getPanelRaster() {
        return panelRaster;
    }

    /**
     * @return the aFiguras
     */
    public ArrayList<Figura> getAFiguras() {
        return aFiguras;
    }

}
