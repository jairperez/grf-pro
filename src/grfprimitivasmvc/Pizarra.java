package grfprimitivasmvc;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import modelos.Raster;
import vistas.PanelPrincipal;

/**
 *
 * @author jarv
 */
public class Pizarra extends javax.swing.JFrame {

    private static final int ANCHO = 800;
    private static final int ALTO = 600;

    public Pizarra() {
        super("Pizarra");
        initComponents();
        this.getContentPane().add(new PanelPrincipal(new Raster(ANCHO, ALTO)));
        /*El tamaño de la ventana debe ser igual al establecido por el raster,
        de otra forma obtenemos la siguiente
        excepcion: ArrayIndexOutOfBoundsException debido a que los pixeles (coordenadas)
        no se encuentran dentro de los limites*/
        this.setBounds(200, 100, ANCHO, ALTO);
//        this.setResizable(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                nimbus();
                new Pizarra().setVisible(true);
            }
        });
    }

    public static void nimbus() {
        try {
            UIManager.setLookAndFeel(
                    "javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            System.out.println("Error!!!");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
