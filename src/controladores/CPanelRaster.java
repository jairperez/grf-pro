package controladores;

import modelos.Vertex2D;
import java.awt.*;
import java.awt.event.*;
import javax.swing.ButtonGroup;
import modelos.Raster;
import modelos.figuras.*;
import vistas.*;

public class CPanelRaster extends MouseAdapter {

    private final PanelRaster panelRaster;
    private final PanelPrincipal panelPrincipal;
    private final Raster raster;
    private final PanelControles panelControles;
    private final PanelFiguras panelFiguras;
    private final Point p1, p2, p3;
    private boolean bP1, bP2, bP3;
    public static final int LINEA = 0;
    public static final int TRIANGULO = 1;
    public static final int RECTANGULO = 2;
    public static final int CIRCULO = 3;
    public static final int ELIPSE = 4;
    private int figura = LINEA;

    public CPanelRaster(Raster raster, PanelPrincipal panelPrincipal) {
        panelRaster = panelPrincipal.getPanelRaster();
        panelControles = panelPrincipal.getPanelControles();
        panelFiguras = panelPrincipal.getPanelFiguras();
        this.panelPrincipal = panelPrincipal;

        p1 = new Point();
        p2 = new Point();
        p3 = new Point();

        bP1 = false;
        bP2 = false;
        bP3 = false;

        this.raster = raster;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        dibujarFigura(e);
    }

    private void dibujarFigura(MouseEvent evt) {

        Color color = panelControles.getColor();
        // TODO add your handling code here:

        establecerFigura();

        if (figura == TRIANGULO && bP1 && bP2 && !bP3) {
            p3.x = evt.getX();
            p3.y = evt.getY();
            bP3 = true;
        }

        if (bP1 && !bP2) {
            p2.x = evt.getX();
            p2.y = evt.getY();
            bP2 = true;
        }

        if (!bP1) {
            p1.x = evt.getX();
            p1.y = evt.getY();
            bP1 = true;
        }

        if (figura == LINEA && bP1 && bP2) {
            dibujarLinea(p1, p2, color);

            bP1 = false;
            bP2 = false;
            bP3 = false;
        }

        if (figura == TRIANGULO && bP1 && bP2 && bP3) {
            dibujarTriangulo(p1, p2, p3, color);

            bP1 = false;
            bP2 = false;
            bP3 = false;
        }

        if (figura == RECTANGULO && bP1 && bP2) {
            dibujarCuadrado(p1, p2, color);

            bP1 = false;
            bP2 = false;
            bP3 = false;
        }
        
        if (figura == CIRCULO && bP1 && bP2) {
            dibujarCirculo(p1, p2, color);

            bP1 = false;
            bP2 = false;
            bP3 = false;
        }
        
        if (figura == ELIPSE && bP1 && bP2) {
            dibujarElipse(p1, p2, color);
            bP1 = false;
            bP2 = false;
            bP3 = false;
        }

    }
    
    private void dibujarCirculo(Point p1, Point p2, Color c) {
        Circulo circulo = new Circulo(p1, p2, c);
        circulo.drawCircleBresenhamInt(raster);
        
        panelPrincipal.getAFiguras().add(circulo);
        panelFiguras.getListVectoresModel().addElement("Circulo");
        
        panelRaster.repaint();
    }
    
    private void dibujarElipse(Point p1, Point p2, Color c) {
        Elipse elipse = new Elipse(p1, p2, c);
        elipse.drawEllipseBresenham(raster);
        
        panelPrincipal.getAFiguras().add(elipse);
        panelFiguras.getListVectoresModel().addElement("Elipse");
        
        panelRaster.repaint();
    }

    private void dibujarTriangulo(Point p1, Point p2, Point p3, Color c) {
        // TODO add your handling code here:
        Vertex2D v1 = new Vertex2D(p1.x, p1.y, c.getRGB());
        Vertex2D v2 = new Vertex2D(p2.x, p2.y, c.getRGB());
        Vertex2D v3 = new Vertex2D(p3.x, p3.y, c.getRGB());

        TrianguloR tri = new TrianguloR(v1, v2, v3, c);

        tri.dibujar(raster);

        panelPrincipal.getAFiguras().add(tri);

        panelFiguras.getListVectoresModel().addElement("Triangulo");

        panelRaster.repaint();
    }

    private void dibujarLinea(Point p1, Point p2, Color color) {
        Linea l = new Linea(p1, p2, color);
        l.setRaster(raster);

        panelPrincipal.getAFiguras().add(l);
        panelFiguras.getListVectoresModel().addElement("Linea");

//        long inicio = 0, fin = 0;
        //inicio = System.nanoTime();
        // lineaMejorada(_p1.x,_p1.y,_p2.x,_p2.y,color);
        //fin    = System.nanoTime();
        //System.out.printf("Tiempo transcurrido, simple: %d\n",(fin-inicio));
        //inicio = System.nanoTime();
        l.dibujarLineFast(p1.x, p1.y, p2.x, p2.y, color);
        //fin    = System.nanoTime();            

        //System.out.printf("Tiempo transcurrido, fast  : %d\n",(fin-inicio));
        panelRaster.repaint();
    }

    private void dibujarCuadrado(Point p1, Point p2, Color color) {
        Rectangulo c = new Rectangulo(p1, p2, color);
        c.dibujar(raster);
        panelPrincipal.getAFiguras().add(c);
        panelFiguras.getListVectoresModel().addElement("Rectangulo");
        panelRaster.repaint();
    }

    private void establecerFigura() {
        ButtonGroup grupoBotones = panelControles.getGrupoBotones();
        String figuraSeleccionada = grupoBotones.getSelection().getActionCommand();
        switch (figuraSeleccionada) {
            case "Linea":
                figura = LINEA;
                break;
            case "Triangulo":
                figura = TRIANGULO;
                break;
            case "Rectangulo":
                figura = RECTANGULO;
                break;
            case "Circulo":
                figura = CIRCULO;
                break;
            case "Elipse":
                figura = ELIPSE;
        }
    }

}
